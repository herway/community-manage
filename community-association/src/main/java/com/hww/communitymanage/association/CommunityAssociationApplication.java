package com.hww.communitymanage.association;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.openfeign.FeignClient;

@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients(basePackages = "com.hww.communitymanage.association.feign")
public class CommunityAssociationApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommunityAssociationApplication.class, args);
    }
}
