package com.hww.communitymanage.association.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
@Data
@TableName("ass_communityuser")
public class CommunityuserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 社团ID
	 */
	private String communityId;
	/**
	 * 人员ID
	 */
	private String userId;
	/**
	 * 入社时间
	 */

	private String joinTime;
	/**
	 * 人员状态0退社1正常
	 */
	@TableLogic  //逻辑删除
	private Integer userState;
	/**
	 * 职位0社员1社长
	 */
	private Integer position;
	/**
	 * 审批人
	 */
	private String adminUser;
	/**
	 * 最后修改时间
	 */
	private String updateTime;
	/**
	 * 社团名称
	 */
	private String communityName;
	/**
	 * 拓展字段2
	 */
	private Object expand2;

}
