package com.hww.communitymanage.association.controller;

import java.util.*;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.association.entity.CommunityEntity;
import com.hww.communitymanage.association.entity.CommunitycheckEntity;
import com.hww.communitymanage.association.entity.CommunityuserstreamEntity;
import com.hww.communitymanage.association.service.CommunityService;
import com.hww.communitymanage.association.service.CommunityuserstreamService;
import com.hww.communitymanage.association.utils.RedisUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.association.entity.CommunityuserEntity;
import com.hww.communitymanage.association.service.CommunityuserService;


/**
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
@RestController
@RequestMapping("association/communityuser")
public class CommunityuserController {
    @Autowired
    private CommunityuserService communityuserService;
    //导入redis
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private CommunityController communityController;
    @Autowired
    private CommunityuserstreamService communityuserstreamService;
    @Autowired
    private CommunityService communityService;
    @Autowired
    private RabbitTemplate rabbitTemplate;


    /**
     * rabbit发送消息
     */
    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }

    /**
     * 判断该人员是否在社团内
     */
    @RequestMapping("/isCommunityUser")
    //@RequiresPermissions("association:communityuser:list")
    public Boolean list(@RequestBody Map<String, Object> params) {
        QueryWrapper<CommunityuserEntity> querwrapper = new QueryWrapper<>();
        querwrapper.eq("USER_ID",params.get("userId")).eq("COMMUNITY_ID",params.get("communityId"));
        CommunityuserEntity communityuserEntity = communityuserService.getOne(querwrapper);
       if (communityuserEntity==null){
           return false;
       }else {
           return true;
       }

    }

    /**
     * 查询社团人员列表
     */
    @RequestMapping("/communityUserList")
    //@RequiresPermissions("association:communityuser:list")
    public R list(@RequestParam Map<String, Object> params, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        PageUtils page = communityuserService.queryPage(params);
        List<CommunityuserEntity> stuList = (List<CommunityuserEntity>) page.getList();
        for (int i = 0; i < stuList.size(); i++) {
            String key = Constant.COMMUNITY_USER + stuList.get(i).getUserId();
            if (redisUtil.hasKey(key)) {
                stuList.get(i).setExpand2(JSON.parseObject(String.valueOf(redisUtil.get(key))));
            }
        }
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{recId}")
    //@RequiresPermissions("association:communityuser:info")
    public R info(@PathVariable("recId") String recId) {
        CommunityuserEntity communityuser = communityuserService.getById(recId);

        return R.ok().put("communityuser", communityuser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("association:communityuser:save")
    public R save(@RequestBody CommunityuserEntity communityuser, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        communityuserService.save(communityuser);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("association:communityuser:update")
    public R update(@RequestBody CommunityuserEntity communityuser, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        communityuserService.updateById(communityuser);
        return R.ok();
    }

    /**
     * 删除社团人员
     */
    @RequestMapping("/delete/{recId}")
    //@RequiresPermissions("association:communityuser:delete")
    public R delete(@PathVariable("recId") String recId, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        //添加社团人员流动表

        CommunityuserEntity communityuserEntity = communityuserService.getById(recId);
        CommunityEntity communityEntity = communityController.getCommunityInfo(communityuserEntity.getCommunityId());
        CommunityuserstreamEntity communityuserstreamEntity = communityUserToCommunityuserstreamEntity(communityuserEntity);
        communityuserstreamEntity.setUserCheck(communityEntity.getCommunityPresident());
        communityuserstreamService.save(communityuserstreamEntity);
        String communityId = communityuserEntity.getCommunityId();


        if (Objects.equals(communityEntity.getPeopleState(), 2)) {
            communityEntity.setPeopleState(0);
        }
        communityEntity.setCommunityNumber(communityEntity.getCommunityNumber() - 1);
        communityService.updateById(communityEntity);
        //缓存一致性
        redisUtil.del(Constant.COMMUNITY_ASSOCIATION + communityEntity.getCommunityId());
        communityuserService.removeByIds(Arrays.asList(recId));
        MessageUtli messageUtli = new MessageUtli(communityId, "你已被" + communityEntity.getCommunityName() + "请出社团", "退社通知", communityEntity.getCommunityName(), null, DateUtils.getLocalDateTimeStr(), communityEntity.getCommunityPresident(), communityEntity.getCommunityPresidentName(),
                communityuserEntity.getUserId(), null, null, 0);
        sendMsg(messageUtli);
        return R.ok();
    }

    //社团人员转换为社团人员流动对象
    public CommunityuserstreamEntity communityUserToCommunityuserstreamEntity(CommunityuserEntity communityuserEntity) {
        CommunityuserstreamEntity communityuserstreamEntity = new CommunityuserstreamEntity();
        communityuserstreamEntity.setCommunityId(communityuserEntity.getCommunityId());
        communityuserstreamEntity.setUserId(communityuserEntity.getUserId());
        communityuserstreamEntity.setApplyState(1);
        communityuserstreamEntity.setJoinTime(DateUtils.getLocalDateTimeStr());
        communityuserstreamEntity.setApplyTime(DateUtils.getLocalDateTimeStr());
        communityuserstreamEntity.setPosition(0);
        communityuserstreamEntity.setOpState(2);
        communityuserstreamEntity.setCommunityName(communityuserEntity.getCommunityName());
        return communityuserstreamEntity;
    }
}
