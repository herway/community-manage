package com.hww.communitymanage.association.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
@Data
@TableName("ass_community")
public class CommunityEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 社团ID
	 */
	private String communityId;
	/**
	 * 社团名称
	 */
	private String communityName;
	/**
	 * 社团简介
	 */
	private String communityIntroduce;
	/**
	 * 社团图片
	 */
	private String communityImage;
	/**
	 * 社团附件
	 */
	private String applyUrl;
	/**
	 * 招聘状态 0 不招聘 1 招聘 2 招聘人数满
	 */
	private Integer peopleState;
	/**
	 * 创建人
	 */
	private String communityCreater;
	/**
	 * 社长
	 */
	private String communityPresident;
	/**
	 * 创建人姓名
	 */
	private String communityCreaterName;
	/**
	 * 社长姓名
	 */
	private String communityPresidentName;
	/**
	 * 社团人数
	 */
	private Integer communityNumber;
	/**
	 * 社团招聘人数
	 */
	private Integer maxUser;
	/**
	 * 社团申请理由
	 */
	private String applyAdvice;
	/**
	 * 通过时间
	 */
	private String createTime;
	/**
	 * 申请建社时间
	 */
	private String communityApplyTime;
	/**
	 * 换届次数
	 */
	private Integer chanageNum;
	/**
	 * 社团进展状态0发起1管理员处理2超级管理员处理3结束
	 */
	private Integer developState;
	/**
	 * 最后更新时间
	 */
	private String updateTime;
	/**
	 * 社团热度
	 */
	private Integer hotNum;
	/**
	 * 是否展示0不展示1展示
	 */
	@TableLogic  //逻辑删除
	private Integer showState;
	/**
	 * 拓展字段1
	 */
	private Integer expand1;
	/**
	 * 拓展字段2
	 */
	@TableField(select = false)   //查询不返回此字段
	private String expand2;
	/**
	 * 换届次数
	 */
	private Integer version;

}
