package com.hww.communitymanage.association.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.association.entity.CommunityEntity;
import com.hww.communitymanage.association.entity.CommunitycheckEntity;
import com.hww.communitymanage.association.entity.CommunityuserEntity;
import com.hww.communitymanage.association.service.CommunityService;
import com.hww.communitymanage.association.service.CommunityuserService;
import com.hww.communitymanage.association.utils.RedisUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.association.entity.CommunityuserstreamEntity;
import com.hww.communitymanage.association.service.CommunityuserstreamService;


/**
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:58:09
 */
@RestController
@RequestMapping("association/communityuserstream")
public class CommunityuserstreamController {
    @Autowired
    private CommunityuserstreamService communityuserstreamService;
    @Autowired
    private CommunityuserService communityuserService;
    @Autowired
    private CommunityuserController communityuserController;
    @Autowired
    private CommunityController communityController;
    @Autowired
    private CommunityService communityService;
    //导入redis
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * rabbit发送消息
     */
    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("association:communityuserstream:list")
    public R list(@RequestParam Map<String, Object> params, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        PageUtils page = communityuserstreamService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{recId}")
    //@RequiresPermissions("association:comstream:info")
    public R info(@PathVariable("recId") String recId) {
        CommunityuserstreamEntity communityuserstream = communityuserstreamService.getById(recId);

        return R.ok().put("communityuserstream", communityuserstream);
    }

    /**
     * 保存入退社信息
     */
    @RequestMapping("/save")
    //@RequiresPermissions("association:communityuserstream:save")
    public R save(@RequestBody CommunityuserstreamEntity communityuserstream, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        //判断其是否在申请中 或者已在该社团
        QueryWrapper<CommunityuserstreamEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("COMMUNITY_ID", communityuserstream.getCommunityId()).eq("USER_ID", communityuserstream.getUserId()).eq("APPLY_STATE", 0);
        CommunityuserstreamEntity communityuserstreamEntity = communityuserstreamService.getOne(queryWrapper);
        if (!StringUtils.isEmpty(communityuserstreamEntity)) {
            return R.error(Constant.ERROR, "该社团已申请，请勿重复申请");
        }
        QueryWrapper<CommunityuserEntity> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("COMMUNITY_ID", communityuserstream.getCommunityId()).eq("USER_ID", communityuserstream.getUserId());
        CommunityuserEntity communityuserEntity = communityuserService.getOne(queryWrapper1);
        if (!StringUtils.isEmpty(communityuserEntity) && communityuserstream.getOpState() == 1) {
            return R.error(Constant.ERROR, "你已是该社团成员，请勿重复申请");
        }
        CommunityEntity communityInfo = communityController.getCommunityInfo(communityuserstream.getCommunityId());
        //向社长发送消息
        MessageUtli messageUtli = new MessageUtli(communityuserstream.getCommunityId(), null, null, communityInfo.getCommunityName(), null, DateUtils.getLocalDateTimeStr(), communityuserstream.getUserId(),
                communityuserstream.getUserName(), communityInfo.getCommunityPresident(), communityInfo.getCommunityPresidentName(), null, 5);
        sendMsg(messageUtli);
        communityuserstreamService.save(communityuserstream);
        return R.ok();
    }

    /**
     * 处理入社审批
     */
    @RequestMapping("/update")
    //@RequiresPermissions("association:communityuserstream:update")
    public R update(@RequestBody CommunityuserstreamEntity communityuserstream, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        CommunityuserstreamEntity communityuserstreamEntity = communityuserstreamService.getById(communityuserstream.getRecId());
        communityuserstreamService.updateById(communityuserstream);
        CommunityEntity communityInfo = communityController.getCommunityInfo(communityuserstreamEntity.getCommunityId());
        //审批人员
        if (Objects.equals(1, communityuserstream.getApplyState()) && Objects.equals(1, communityuserstreamEntity.getOpState())) {
            if (Objects.equals(2, communityInfo.getPeopleState())) {
                return R.error(Constant.ERROR, "此社团目前人数已满");
            }
            //社团表目前人数+1
            CommunityEntity communityEntity = new CommunityEntity();
            communityEntity.setRecId(communityInfo.getRecId());
            Integer newCommunityNum = communityInfo.getCommunityNumber() + 1;
            if (Objects.equals(newCommunityNum, communityInfo.getMaxUser())) {
                communityEntity.setPeopleState(2);
                communityEntity.setCommunityNumber(newCommunityNum);
            } else {
                communityEntity.setCommunityNumber(newCommunityNum);
            }
            communityService.updateById(communityEntity);
            //删除redis缓存
            String key = Constant.COMMUNITY_ASSOCIATION + communityuserstreamEntity.getCommunityId();
            redisUtil.del(key);
            //社团人员表新增
            CommunityuserEntity communityuserEntity = userStreamToCommunityUser(communityuserstreamEntity);
            communityuserService.save(communityuserEntity);
            //向申请人发送消息
            MessageUtli messageUtli = new MessageUtli(communityuserstreamEntity.getCommunityId(), null, null, communityInfo.getCommunityName(), null, DateUtils.getLocalDateTimeStr(), communityInfo.getCommunityPresident(), communityInfo.getCommunityPresidentName(), communityuserstreamEntity.getUserId(),
                    communityuserstreamEntity.getUserName(), null, 1);
            sendMsg(messageUtli);
        } else if (1 == communityuserstream.getApplyState() && 0 == communityuserstreamEntity.getOpState()) {
            QueryWrapper<CommunityuserEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("COMMUNITY_ID", communityuserstreamEntity.getCommunityId()).eq("USER_ID", communityuserstreamEntity.getUserId());
            communityuserService.remove(queryWrapper);
            //成功后该社团人数-1
            Integer newCommunityNum = communityInfo.getCommunityNumber() - 1;
            communityInfo.setCommunityNumber(newCommunityNum);
            communityService.updateById(communityInfo);
            //向申请人发送消息
            MessageUtli messageUtli = new MessageUtli(communityuserstreamEntity.getCommunityId(), null, null, communityInfo.getCommunityName(), null, DateUtils.getLocalDateTimeStr(), communityInfo.getCommunityPresident(), communityInfo.getCommunityPresidentName(), communityuserstreamEntity.getUserId(),
                    communityuserstreamEntity.getUserName(), null, 3);
            sendMsg(messageUtli);
        } else if (2 == communityuserstream.getApplyState()) {
            //向申请人发送消息
            MessageUtli messageUtli = new MessageUtli(communityuserstream.getCommunityId(), null, "社团拒绝通知", communityInfo.getCommunityName(), null, DateUtils.getLocalDateTimeStr(), communityInfo.getCommunityPresident(), communityInfo.getCommunityPresidentName(), communityuserstreamEntity.getUserId(),
                    communityuserstreamEntity.getUserName(), null, 3);
            sendMsg(messageUtli);
        }

        return R.ok();
    }

    private CommunityuserEntity userStreamToCommunityUser(CommunityuserstreamEntity communityuserstreamEntity) {
        CommunityuserEntity communityuserEntity = new CommunityuserEntity();
        communityuserEntity.setCommunityId(communityuserstreamEntity.getCommunityId());
        communityuserEntity.setUserId(communityuserstreamEntity.getUserId());
        communityuserEntity.setJoinTime(DateUtils.getLocalDateTimeStr());
        communityuserEntity.setCommunityName(communityuserstreamEntity.getCommunityName());
        communityuserEntity.setAdminUser(communityuserstreamEntity.getUserCheck());
        return communityuserEntity;
    }


}
