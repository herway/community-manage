package com.hww.communitymanage.association.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.association.dao.CommunityDao;
import com.hww.communitymanage.association.entity.CommunityEntity;
import com.hww.communitymanage.association.service.CommunityService;
import org.springframework.util.StringUtils;


@Service("communityService")
public class CommunityServiceImpl extends ServiceImpl<CommunityDao, CommunityEntity> implements CommunityService {
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<CommunityEntity> querwrapper = new QueryWrapper<>();

        if (!StringUtils.isEmpty(params.get("developState"))){
            querwrapper.eq("DEVELOP_STATE",params.get("developState"));
        }
        if (!StringUtils.isEmpty(params.get("communityName"))){
            querwrapper.eq("COMMUNITY_NAME",params.get("communityName"));
        }
        if (!StringUtils.isEmpty(params.get("peopleState"))){
            querwrapper.eq("PEOPLE_STATE",params.get("peopleState"));
        }

            IPage<CommunityEntity> page = this.page(
                    new Query<CommunityEntity>().getPage(params),
                    querwrapper
            );
        return new PageUtils(page);
    }
    
}