package com.hww.communitymanage.rule.service.impl;

import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.rule.dao.RuleDao;
import com.hww.communitymanage.rule.entity.RuleEntity;
import com.hww.communitymanage.rule.service.RuleService;


@Service("ruleService")
public class RuleServiceImpl extends ServiceImpl<RuleDao, RuleEntity> implements RuleService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<RuleEntity> page = this.page(
                new Query<RuleEntity>().getPage(params),
                new QueryWrapper<RuleEntity>()
        );

        return new PageUtils(page);
    }

}