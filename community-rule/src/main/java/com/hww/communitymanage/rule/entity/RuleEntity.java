package com.hww.communitymanage.rule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 社团规则表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-27 02:30:09
 */
@Data
@TableName("rul_rule")
public class RuleEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
    private String recId;
    /**
     * 规则内容
     */
    private String communityRule;
    /**
     * 更新时间
     */
    private String updateTime;

}
