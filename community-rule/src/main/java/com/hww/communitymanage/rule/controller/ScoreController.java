package com.hww.communitymanage.rule.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hww.communitymanage.rule.entity.ScoreEntity;
import com.hww.communitymanage.rule.service.ScoreService;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.R;


/**
 * 分数级别表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-27 02:30:09
 */
@RestController
@RequestMapping("rule/score")
public class ScoreController {
    @Autowired
    private ScoreService scoreService;

    /**
     * 列表
     */
    @RequestMapping("/list/tree")
    public R list() {
        List<ScoreEntity> resultLists = scoreService.listWithTree();
        // 组装成父子结构
     /*   List<CategoryEntity> collect = resultLists.stream().filter((CategoryEntity) -> {
           return CategoryEntity.getParentCid() == 0; }
       ).collect(Collectors.toList());*/   //完整写法   下面是省略写法
        List<ScoreEntity> leve1 = resultLists.stream().filter(CategoryEntity ->
                CategoryEntity.getParentCid() == 0
        ).map((meau) ->
        {
            meau.setChildren(getChildrens(meau, resultLists));
            return meau;
        }).collect(Collectors.toList());

        return R.ok().put("data", leve1);
    }

    //递归查找所有菜单的子菜单
    private List<ScoreEntity> getChildrens(ScoreEntity now, List<ScoreEntity> all) {
        List<ScoreEntity> result = all.stream().filter(categoryEntity ->
        {
            return categoryEntity.getParentCid() == now.getLevelId();
        }).map(categoryEntity ->
        {
            categoryEntity.setChildren(getChildrens(categoryEntity, all));
            return categoryEntity;
        }).collect(Collectors.toList());
        return result;
    }

    /**
     * 信息
     */
    @RequestMapping("/list")
        public R list(@RequestParam Map<String, Object> params) {

        List<ScoreEntity> scoreEntities = scoreService.listByMap(params);
        return R.ok().put("data", scoreEntities);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody ScoreEntity score) {
        scoreService.save(score);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody ScoreEntity score) {
        scoreService.updateById(score);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] levelIds) {
        scoreService.removeByIds(Arrays.asList(levelIds));

        return R.ok();
    }

}
