package com.hww.communitymanage.rule.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hww.communitymanage.rule.entity.SchoolEntity;
import com.hww.communitymanage.rule.service.SchoolService;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.R;


/**
 * 学校简介表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-27 02:30:09
 */
@RestController
@RequestMapping("rule/school")
public class SchoolController {
    @Autowired
    private SchoolService schoolService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = schoolService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{recId}")
    public R info(@PathVariable("recId") String recId) {
        SchoolEntity school = schoolService.getById(recId);

        return R.ok().put("school", school);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody SchoolEntity school) {
        schoolService.save(school);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody SchoolEntity school) {
        schoolService.updateById(school);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] recIds) {
        schoolService.removeByIds(Arrays.asList(recIds));

        return R.ok();
    }

}
