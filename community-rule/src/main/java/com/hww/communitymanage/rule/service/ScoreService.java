package com.hww.communitymanage.rule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hww.common.utils.PageUtils;
import com.hww.communitymanage.rule.entity.ScoreEntity;

import java.util.List;
import java.util.Map;

/**
 * 分数级别表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-27 02:30:09
 */
public interface ScoreService extends IService<ScoreEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<ScoreEntity> listWithTree();
}

