package com.hww.communitymanage.rule.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 分数级别表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-27 02:30:09
 */
@Data
@TableName("rul_score")
public class ScoreEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 活动级别ID
	 */
	@TableId
	private Long levelId;
	/**
	 * 级别名称
	 */
	private String scoreName;
	/**
	 * 父分类id
	 */
	private Long parentCid;
	/**
	 * 层级
	 */
	private Integer scoreLevel;
	/**
	 * 是否显示[0-不显示，1显示]
	 */
	private Integer showStatus;
	/**
	 * 更新时间
	 */
	private String updateTime;
	/**
	 *
	 */
	private Double score;

	@TableField(exist = false)
	private List<ScoreEntity> children;

}
