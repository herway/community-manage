package com.hww.communitymanage.rule.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hww.communitymanage.rule.entity.RuleEntity;
import com.hww.communitymanage.rule.service.RuleService;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.R;


/**
 * 社团规则表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-27 02:30:09
 */
@RestController
@RequestMapping("rule/rule")
public class RuleController {
    @Autowired
    private RuleService ruleService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = ruleService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{recId}")
    public R info(@PathVariable("recId") String recId) {
        RuleEntity rule = ruleService.getById(recId);

        return R.ok().put("rule", rule);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody RuleEntity rule) {
        ruleService.save(rule);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody RuleEntity rule) {
        ruleService.updateById(rule);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody String[] recIds) {
        ruleService.removeByIds(Arrays.asList(recIds));

        return R.ok();
    }

}
