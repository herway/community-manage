package com.hww.communitymanage.rule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hww.common.utils.PageUtils;
import com.hww.communitymanage.rule.entity.RuleEntity;

import java.util.Map;

/**
 * 社团规则表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-27 02:30:09
 */
public interface RuleService extends IService<RuleEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

