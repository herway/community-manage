package com.hww.communitymanage.activity.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
@Data
@TableName("act_activityuser")
public class ActivityuserEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 活动id
	 */
	private String activityId;
	/**
	 * 活动名称
	 */
	private String activityName;
	/**
	 * 人员ID
	 */
	private String userId;
	/**
	 * 是否得分0不得分1得分
	 */
	private Integer scoreState;
	/**
	 * 人员姓名
	 */
	private String userName;
	/**
	 * 加入时间
	 */
	private String joinTime;
	/**
	 * 活动信息
	 */
	private Object expand1;
	/**
	 * 学生信息
	 */
	private Object expand2;
	/**
	 * 活动人员信息
	 */
	private Integer IS_SCORE;

}
