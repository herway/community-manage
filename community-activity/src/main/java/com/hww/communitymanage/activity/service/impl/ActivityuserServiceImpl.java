package com.hww.communitymanage.activity.service.impl;

import com.hww.communitymanage.activity.entity.ActivityEntity;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.activity.dao.ActivityuserDao;
import com.hww.communitymanage.activity.entity.ActivityuserEntity;
import com.hww.communitymanage.activity.service.ActivityuserService;
import org.springframework.util.StringUtils;


@Service("activityuserService")
public class ActivityuserServiceImpl extends ServiceImpl<ActivityuserDao, ActivityuserEntity> implements ActivityuserService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ActivityuserEntity> querwrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("activityId"))){
            querwrapper.eq("ACTIVITY_ID",params.get("activityId"));
        }
        if (!StringUtils.isEmpty(params.get("userId"))){
            querwrapper.eq("USER_ID",params.get("userId"));
        }

        IPage<ActivityuserEntity> page = this.page(
                new Query<ActivityuserEntity>().getPage(params),
               querwrapper
        );

        return new PageUtils(page);
    }

}