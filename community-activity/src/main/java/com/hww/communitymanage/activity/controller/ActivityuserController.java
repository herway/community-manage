package com.hww.communitymanage.activity.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.hww.common.utils.*;
import com.hww.communitymanage.activity.config.RedisUtil;
import com.hww.communitymanage.activity.entity.ActivityEntity;
import com.hww.communitymanage.activity.entity.ActivityuserstreamEntity;
import com.hww.communitymanage.activity.service.ActivityuserstreamService;
import jodd.util.StringUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.activity.entity.ActivityuserEntity;
import com.hww.communitymanage.activity.service.ActivityuserService;


/**
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 15:53:07
 */
@RestController
@RequestMapping("activity/activityuser")
public class ActivityuserController {
    @Autowired
    private ActivityuserService activityuserService;
    @Autowired
    private ActivityuserstreamService activityuserstreamService;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    ActivityController activityController;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * rabbit发送消息
     */
    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("activity:activityuser:list")
    public R list(@RequestParam Map<String, Object> params,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        PageUtils page = activityuserService.queryPage(params);
        List<ActivityuserEntity> stuList = (List<ActivityuserEntity>) page.getList();
        for (int i = 0; i < stuList.size(); i++) {
            String key = Constant.COMMUNITY_USER + stuList.get(i).getUserId();
                stuList.get(i).setExpand2(JSON.parseObject(String.valueOf(redisUtil.get(key))));
                stuList.get(i).setExpand1(activityController.getActivityInfo( stuList.get(i).getActivityId()));
        }
        if (StringUtils.isEmpty(params.get("userId"))){
            return R.ok().put("page", page);
        }else {
            return R.ok().put("page", page).put("userScore",redisUtil.get(Constant.COMMUNITY_USER_SCORE+params.get("userId")));
        }

    }


    /**
     * 信息
     */
    @RequestMapping("/info/{recId}")
    //@RequiresPermissions("activity:activityuser:info")
    public R info(@PathVariable("recId") String recId) {
        ActivityuserEntity activityuser = activityuserService.getById(recId);

        return R.ok().put("activityuser", activityuser);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("activity:activityuser:save")
    public R save(@RequestBody ActivityuserEntity activityuser,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        activityuserService.save(activityuser);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("activity:activityuser:update")
    public R update(@RequestBody ActivityuserEntity activityuser,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        activityuserService.updateById(activityuser);
        return R.ok();
    }

    /**
     * 删除活动人员
     */
    @RequestMapping("/delete/{recId}")
    //@RequiresPermissions("activity:activityuser:delete")
    public R delete(@PathVariable("recId") String recId,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }

        ActivityuserEntity activityuserEntity = activityuserService.getById(recId);
        ActivityEntity activityInfo = activityController.getActivityInfo(activityuserEntity.getActivityId());
        ActivityuserstreamEntity activityuserstreamEntity = activityUserToactivityUserStream(activityuserEntity);
        activityuserstreamEntity.setOpUser(activityInfo.getApplyUserId());
        activityuserstreamEntity.setOpName(activityInfo.getApplyUserName());
        activityuserstreamService.save(activityuserstreamEntity);
        activityuserService.removeByIds(Arrays.asList(recId));
        //创建消息类对象
        MessageUtli messageUtli = new MessageUtli(activityInfo.getCreateOrganizationId(), activityInfo.getCreateOrganizationName() + "已将您移出" + activityInfo.getActivityName() + "活动", "删除通知", activityInfo.getCreateOrganizationName(), activityInfo.getActivityId(), DateUtils.getLocalDateTimeStr(),
                null, null, activityuserEntity.getUserId(), activityuserEntity.getUserName(), activityInfo.getActivityName(), 0);
        sendMsg(messageUtli);
        return R.ok();
    }

    //活动人员对象转活动人员流动对象
    private ActivityuserstreamEntity activityUserToactivityUserStream(ActivityuserEntity activityuserEntity) {
        ActivityuserstreamEntity activityuserstreamEntity = new ActivityuserstreamEntity();
        activityuserstreamEntity.setActivityId(activityuserEntity.getActivityId());
        activityuserstreamEntity.setActivityName(activityuserEntity.getActivityName());
        activityuserstreamEntity.setDealState(2);
        activityuserstreamEntity.setApplyType(2);
        activityuserstreamEntity.setOpTime(DateUtils.getLocalDateTimeStr());
        activityuserstreamEntity.setUserId(activityuserEntity.getUserId());
        activityuserstreamEntity.setUserName(activityuserEntity.getUserName());
        activityuserstreamEntity.setApplyTime(DateUtils.getLocalDateTimeStr());
        return activityuserstreamEntity;
    }

}
