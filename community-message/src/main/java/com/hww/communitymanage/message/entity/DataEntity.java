package com.hww.communitymanage.message.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:04:08
 */
@Data
@TableName("msg_data")
public class DataEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 文件ID
	 */
	private String fileId;
	/**
	 * 文件类型0图片1文本2WORD3压缩包
	 */
	private Integer fileType;
	/**
	 * 文件大小
	 */
	private Double fileSize;
	/**
	 * 文件名称
	 */
	private String fileName;
	/**
	 * 拓展字段1
	 */
	@TableField(select = false)   //查询不返回此字段
	private String expand1;
	/**
	 * 拓展字段2
	 */
	@TableField(select = false)   //查询不返回此字段
	private String expand2;

}
