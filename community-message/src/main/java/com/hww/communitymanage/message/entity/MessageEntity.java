package com.hww.communitymanage.message.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:04:08
 */

@Data
@TableName("msg_message")
public class MessageEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 留言内容
	 */
	private String content;
	/**
	 * 留言标题
	 */
	private String title;
	/**
	 * 留言时间
	 */
	private String creatTime;
	/**
	 * 留言人
	 */
	private String userId;
	/**
	 * 留言人姓名
	 */
	private String userName;
	/**
	 * 消息类型  0留言 1公告
	 */
	private Integer msgType;
	/**
	 * 拓展字段2
	 */
	@TableField(select = false)   //查询不返回此字段
	private String expand2;

}
