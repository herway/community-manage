package com.hww.communitymanage.message.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.message.dao.MessageDao;
import com.hww.communitymanage.message.entity.MessageEntity;
import com.hww.communitymanage.message.service.MessageService;
import org.springframework.util.StringUtils;


@Service("messageService")
public class MessageServiceImpl extends ServiceImpl<MessageDao, MessageEntity> implements MessageService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<MessageEntity> queryWrapper=new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("title"))){
            queryWrapper.like("TITLE",params.get("title"));
        }
        if (!StringUtils.isEmpty(params.get("msgType"))){
            queryWrapper.like("MSG_TYPE",params.get("msgType"));
        }
        IPage<MessageEntity> page = this.page(
                new Query<MessageEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

}