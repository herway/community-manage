package com.hww.communitymanage.message.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.hww.common.utils.Constant;
import com.hww.communitymanage.message.config.RedisUtil;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.message.entity.AlertsEntity;
import com.hww.communitymanage.message.service.AlertsService;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.R;


/**
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:04:08
 */
//@RabbitListener(queues = {"QUEUE_MSG"})
@RestController
@RequestMapping("message/alerts")
public class AlertsController {
    @Autowired
    private AlertsService alertsService;
    @Autowired
    RedisUtil redisUtil;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("message:alerts:list")
    public R list(@RequestParam Map<String, Object> params, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        PageUtils page = alertsService.queryPage(params);
        redisUtil.del(Constant.NEW_MESSAGE + params.get("acceptUserId"));
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{recId}")
    //@RequiresPermissions("message:alerts:info")
    public R info(@PathVariable("recId") String recId) {
        AlertsEntity alerts = alertsService.getById(recId);

        return R.ok().put("alerts", alerts);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("message:alerts:save")
    public R save(@RequestBody AlertsEntity alerts, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        alertsService.save(alerts);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("message:alerts:update")
    public R update(@RequestBody List<AlertsEntity> alerts, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        String recId = alerts.get(0).getRecId();
        AlertsEntity alertsEntity = alertsService.getById(recId);
        for (int i = 0; i < alerts.size(); i++) {
            if (alerts.get(i).getIsRead() != null && alerts.get(i).getIsRead() == 1) {
                redisUtil.decr(Constant.NO_READ_MESSAGE + alertsEntity.getAcceptUserId(), 1);
            }
        }
        if ((int) redisUtil.get(Constant.NO_READ_MESSAGE + alertsEntity.getAcceptUserId()) < 0) {
            redisUtil.set(Constant.NO_READ_MESSAGE + alertsEntity.getAcceptUserId(), 0);
        }
        alertsService.updateBatchById(alerts);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("message:alerts:delete")
    public R delete(@RequestBody String[] recIds, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        alertsService.removeByIds(Arrays.asList(recIds));
        return R.ok();
    }

}
