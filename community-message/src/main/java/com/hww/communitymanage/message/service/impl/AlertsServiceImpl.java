package com.hww.communitymanage.message.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.message.dao.AlertsDao;
import com.hww.communitymanage.message.entity.AlertsEntity;
import com.hww.communitymanage.message.service.AlertsService;
import org.springframework.util.StringUtils;


@Service("alertsService")
public class AlertsServiceImpl extends ServiceImpl<AlertsDao, AlertsEntity> implements AlertsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
         QueryWrapper<AlertsEntity> queryWrapper=new QueryWrapper<>();
         if (!StringUtils.isEmpty(params.get("stateId"))){
             queryWrapper.eq("STATE_ID",params.get("stateId"));
         }
         if (!StringUtils.isEmpty(params.get("isRead"))){
             queryWrapper.eq("IS_READ",params.get("isRead"));
         }
         if (!StringUtils.isEmpty(params.get("adminApplyId"))){
             queryWrapper.eq("ADMIN_APPLY_ID",params.get("adminApplyId")).or().eq("ADMIN_APPLY_ID",null);
         }
        queryWrapper.eq("ACCEPT_USER_ID",params.get("acceptUserId"));
        IPage<AlertsEntity> page = this.page(
                new Query<AlertsEntity>().getPage(params),
queryWrapper
        );

        return new PageUtils(page);
    }

}