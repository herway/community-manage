package com.hww.communitymanage.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hww.common.utils.PageUtils;
import com.hww.communitymanage.message.entity.AlertsEntity;

import java.util.Map;

/**
 * 
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:04:08
 */
public interface AlertsService extends IService<AlertsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

