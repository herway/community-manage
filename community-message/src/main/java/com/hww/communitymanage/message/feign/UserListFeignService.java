package com.hww.communitymanage.message.feign;


import com.hww.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;


/**
 * 这是一个声明式的远程调用
 */
@FeignClient("community-user")
public interface UserListFeignService {
    @RequestMapping("user/students/userList")
    public List getUserList();
}
