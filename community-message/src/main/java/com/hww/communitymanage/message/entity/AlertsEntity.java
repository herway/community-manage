package com.hww.communitymanage.message.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:04:08
 */
@Data
@TableName("msg_alerts")
public class AlertsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 社团ID
	 */
	private String communityId;
	/**
	 * 社团姓名
	 */
	private String communityName;
	/**
	 * 活动ID
	 */
	private String acvitityId;
	/**
	 * 消息发送时间
	 */
	private String sendTime;
	/**
	 * 消息接收时间
	 */
	private String acceptTime;
	/**
	 * 发送人ID
	 */
	private String sendUserId;
	/**
	 * 发送人姓名
	 */
	private String sendName;
	/**
	 * 收件人ID
	 */
	private String acceptUserId;
	/**
	 * 收件人姓名
	 */
	private String acceptName;
	/**
	 * 活动姓名
	 */
	private String acvitityName;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 消息标题
	 */
	private String title;
	/**
	 * 是否已读
	 */
	private Integer isRead;
	/**
	 * 是否已读
	 */
	private String adminApplyId;
	/**
	 *1.入社审批进度（发起人）2.活动类审批进度（发起人）3.社团类审批进度（发起人）4.二课分审批进度（发起人）5.入社审批处理（社长）
	 * 6.社团活动处理（社长）7.社团类消息（管理员）8.活动类消息（管理员）9.二课分申请信息（管理员）10.社团类消息（超级管理员）11.活动类消息（超级管理员）
	 * 12.二课分申请信息（超级管理员）13.用户留言（超级管理员）14.全员通知（所有）
	 */
	private Integer stateId;
	/**
	 * 拓展字段2
	 */
	private String expand2;

}
