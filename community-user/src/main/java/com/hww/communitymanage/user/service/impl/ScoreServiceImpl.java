package com.hww.communitymanage.user.service.impl;

import com.hww.communitymanage.user.entity.StudentsEntity;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.user.dao.ScoreDao;
import com.hww.communitymanage.user.entity.ScoreEntity;
import com.hww.communitymanage.user.service.ScoreService;
import org.springframework.util.StringUtils;


@Service("scoreService")
public class ScoreServiceImpl extends ServiceImpl<ScoreDao, ScoreEntity> implements ScoreService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ScoreEntity> querwrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("applyUserId"))){
            querwrapper.like("APPLY_USER_ID",params.get("applyUserId"));
        }
        if (!StringUtils.isEmpty(params.get("opUserId"))){
            querwrapper.like("OP_USER_ID",params.get("opUserId"));
        }
        if (!StringUtils.isEmpty(params.get("developState"))){
            querwrapper.like("DEVELOP_STATE",params.get("developState"));
        }
        IPage<ScoreEntity> page = this.page(
                new Query<ScoreEntity>().getPage(params),
                querwrapper
        );

        return new PageUtils(page);
    }

}