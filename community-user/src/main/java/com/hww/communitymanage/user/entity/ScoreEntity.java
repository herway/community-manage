package com.hww.communitymanage.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 学生分数审批表
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@Data
@TableName("user_score_check")
public class ScoreEntity implements Serializable {
	private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
    /**
     * 申请人学号
     */
    private String applyUserId;
    /**
     * 申请人姓名
     */
    private String applyUserName;
	/**
	 * 申请原因
	 */
	private String applyAdvice;
	/**
	 * 活动ID
	 */
	private String activityId;
	/**
	 * 活动名称
	 */
	private String activityName;
	/**
	 * 申请添加/减去分数
	 */
	private double addScore;
	/**
	 * 审批人ID
	 */
	private String opUserId;
	/**
	 * 审批人姓名
	 */
	private String opUserName;
	/**
	 * 最后修改时间
	 */
	private String updateTime;
	/**
	 * 管理员操作时间
	 */
	private String opTime;
	/**
	 * 超级管理员操作时间
	 */
	private String superOpTime;
	/**
	 * 申请时间
	 */
	private String applyTime;
	/**
	 * 审批意见
	 */
	private String advice;
	/**
	 * 超级管理员审批意见
	 */
	private String superAdvice;
	/**
	 * 管理员分数审批状态0未处理1同意2拒绝
	 */
	private Integer adminState;
	/**
	 * 超级管理员分数审批状态0未处理1同意2拒绝
	 */
	private Integer superAdminState;
	/**
	 * 活动进展状态0发起1管理员处理2超级管理员处理3结束
	 */
	private Integer developState;
    /**
     * 申请证明材料附件
     */
    private String url;
    /**
     * 拓展字段2
     */
    private Object expand2;

}
