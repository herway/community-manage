package com.hww.communitymanage.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hww.common.utils.PageUtils;
import com.hww.communitymanage.user.entity.SuperadminEntity;

import java.util.Map;

/**
 * 超级管理员表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
public interface SuperadminService extends IService<SuperadminEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

