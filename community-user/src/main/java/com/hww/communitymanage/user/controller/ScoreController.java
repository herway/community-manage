package com.hww.communitymanage.user.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.user.config.RedisUtil;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.user.entity.ScoreEntity;
import com.hww.communitymanage.user.service.ScoreService;


/**
 * 学生分数审批表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@RestController
@RequestMapping("user/score")
public class ScoreController {
    @Autowired
    private ScoreService scoreService;
    //导入redis
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * rabbit发送消息
     */

    private void sendMsg(Object msg) {
        String routingKey = "baseMsg";
        String content = JSON.toJSONString(msg);
        rabbitTemplate.convertAndSend(routingKey, content);
    }
    /**
     * 查询二课分审批列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("user:score:list")
    public R list(@RequestParam Map<String, Object> params,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        PageUtils page = scoreService.queryPage(params);
        List<ScoreEntity> list = (List<ScoreEntity>) page.getList();
        for (int i = 0; i < list.size(); i++) {
            if (redisUtil.hasKey(Constant.COMMUNITY_ACTIVITY + list.get(i).getActivityId())) {
                list.get(i).setExpand2(JSON.parseObject(String.valueOf(redisUtil.get(Constant.COMMUNITY_ACTIVITY+list.get(i).getActivityId()))));
            }

        }
        return R.ok().put("page", page);
    }


    /**
     * 根据主键查询审批信息
     */
    @RequestMapping("/info/{recId}")
    //@RequiresPermissions("user:score:info")
    public R info(@PathVariable("recId") String recId,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        ScoreEntity score = scoreService.getById(recId);
        return R.ok().put("score", score);
    }

    /**
     * 新增学生二课分审批
     */
    @RequestMapping("/save")
    //@RequiresPermissions("user:score:save")
    public R save(@RequestBody ScoreEntity score,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        QueryWrapper<ScoreEntity> querwrapper = new QueryWrapper<>();
        querwrapper.eq("APPLY_USER_ID", score.getApplyUserId()).ne("DEVELOP_STATE", 3).eq("ACTIVITY_ID", score.getActivityId());
        ScoreEntity scoreEntity = scoreService.getOne(querwrapper);
        if (scoreEntity != null && scoreEntity.getSuperAdminState()==1) {
            return R.error(Constant.ERROR, "您的此活动已成功加分，请勿重复提交");
        }else if (scoreEntity != null && scoreEntity.getSuperAdminState()==0){
            return R.error(Constant.ERROR, "您有分数请求正在处理中，请勿重复提交");
        }
        //创建消息类对象
        MessageUtli messageUtli = new MessageUtli(null, null, null, null, null, DateUtils.getLocalDateTimeStr(),
                null, null, "admin", "admin", null, 9);
        sendMsg(messageUtli);
        scoreService.save(score);
        return R.ok();
    }

    /**
     * 学生二课分审批
     */
    @RequestMapping("/update")
    //@RequiresPermissions("user:score:update")
    public R update(@RequestBody ScoreEntity score,@RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN+token))){
            return R.error(Constant.TOKEN,"登录信息失效，请重新登陆");
        }
        ScoreEntity scoreEntity = scoreService.getById(score);
        //创建消息类对象
        MessageUtli messageUtli = new MessageUtli(null, null, null, null, null, DateUtils.getLocalDateTimeStr(),
                null, null, scoreEntity.getApplyUserId(), scoreEntity.getApplyUserName(), null, 4);
        if (score.getSuperAdminState() == 1) {
            String key = Constant.COMMUNITY_USER_SCORE + scoreEntity.getApplyUserId();
            Double sor = Double.parseDouble(String.valueOf(redisUtil.get(key)));
            redisUtil.set(key, sor + score.getAddScore(), 60 * 60 * 24 * 30 * 12 * 4);
        }
        if (score.getAdminState()==1&&score.getDevelopState()==1){
            messageUtli.setAcceptUserId("superAdmin");
            messageUtli.setAcceptName("superAdmin");
            messageUtli.setStateId(12);
        }
        sendMsg(messageUtli);
        scoreService.updateById(score);
        return R.ok();
    }

//    /**
//     * 删除
//     */
//    @RequestMapping("/delete")
//    //@RequiresPermissions("user:score:delete")
//    public R delete(@RequestBody String[] recIds){
//		scoreService.removeByIds(Arrays.asList(recIds));
//
//        return R.ok();
//    }

}
