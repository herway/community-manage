package com.hww.communitymanage.user.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.Query;

import com.hww.communitymanage.user.dao.StudentsDao;
import com.hww.communitymanage.user.entity.StudentsEntity;
import com.hww.communitymanage.user.service.StudentsService;
import org.springframework.util.StringUtils;


@Service("studentsService")
public class StudentsServiceImpl extends ServiceImpl<StudentsDao, StudentsEntity> implements StudentsService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<StudentsEntity> querwrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(params.get("userId"))){
            querwrapper.like("USER_ID",params.get("userId"));
        }
        if (!StringUtils.isEmpty(params.get("position"))){
            querwrapper.eq("POSITION",params.get("position"));
        }
        if (!StringUtils.isEmpty(params.get("stuName"))){
            querwrapper.like("STU_NAME",params.get("stuName"));
        }

        IPage<StudentsEntity> page = this.page(
                new Query<StudentsEntity>().getPage(params),
                querwrapper
        );

        return new PageUtils(page);
    }

}