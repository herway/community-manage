package com.hww.communitymanage.user.controller;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.TimeUnit;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hww.common.utils.*;
import com.hww.communitymanage.user.config.RedisUtil;
import com.hww.communitymanage.user.entity.UseradminassociatedEntity;
import com.hww.communitymanage.user.service.UseradminassociatedService;
import org.apache.commons.lang.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import com.hww.communitymanage.user.entity.StudentsEntity;
import com.hww.communitymanage.user.service.StudentsService;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.R;


/**
 * 学生表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@RestController
@RequestMapping("user/students")
public class StudentsController {
    @Autowired
    private StudentsService studentsService;
    //导入redis
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    RedissonClient redissonClient;
    @Autowired(required = false)
    Redisson redisson;
    @Autowired
    private UseradminassociatedService useradminassociatedService;
    @Autowired
    private RabbitTemplate rabbitTemplate;


    //定时任务，每小时更新一次学生总数
    @Scheduled(fixedRate = 60 * 60)
    public void updateActivityNm() {
        redisUtil.set(Constant.STUDENT_NUM, studentsService.count(), 60 * 60 * 24);
    }

    /**
     * 查询学生列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("user:students:list")
    public R list(@RequestParam Map<String, Object> params, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        PageUtils page = studentsService.queryPage(params);

        return R.ok().put("page", page);
    }
    /**
     * 根据主键查询学生信息
     */
    @RequestMapping("/info/{recId}")
    public R getTnfo(@PathVariable("recId") String recId){
        StudentsEntity studentsEntity = studentsService.getById(recId);
        if (org.springframework.util.StringUtils.isEmpty(studentsEntity)){
            return R.error(Constant.ERROR,"无此人信息");
        }
        return R.ok().put("stu", studentsEntity);
    }
    /**
     * 查询所有学生信息
     *
     * @return list
     */
    @RequestMapping("/userList")
    public List getUserList() {
        List<StudentsEntity> list = studentsService.list();
        List<String> list1 = new LinkedList<>();
        for (int i = 0; i < list.size(); i++) {
            list1.add(list.get(i).getUserId());
        }
        return list1;
    }

    /**
     * 根据主键获取用户信息
     */

    public StudentsEntity info(String recId) {
        StudentsEntity students = studentsService.getById(recId);
        return students;
    }

    /**
     * 批量导入学生
     */
    @RequestMapping("/save")
    //@RequiresPermissions("user:students:save")
    public R save(@RequestBody StudentsEntity[] students, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        List<StudentsEntity> list = new ArrayList<StudentsEntity>();
        list = Arrays.asList(students);
        try {
            //密码加密后存储
            for (int i = 0; i < students.length; i++) {
                students[i].setStuPassword(new Md5().EncoderByMd5(students[i].getStuPassword()));
                //信息存入redis
                redisUtil.set(Constant.COMMUNITY_USER_RECID + students[i].getRecId(), students[i].getUserId(), 60 * 60 * 24 * 30 * 12 * 4);
                redisUtil.set(Constant.COMMUNITY_USER_SCORE + students[i].getUserId(), 0.00, 60 * 60 * 24 * 30 * 12 * 4);
                redisUtil.set(Constant.COMMUNITY_USER + students[i].getUserId(), JSON.toJSONString(ConUtils.beanToMap(students[i])), 60 * 60 * 24 * 30 * 12 * 4);
            }
            studentsService.saveBatch(list);
        } catch (Exception e) {
            return R.error(1001, "学号重复，请检查数据");
        }

        return R.ok();
    }

    /**
     * 学生修改密码
     */
    @RequestMapping("/updatePws")
    //@RequiresPermissions("user:students:update")
    public R update(@RequestParam Map<String, String> params, @RequestHeader("token") String token) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        String oldPws = params.get("oldPws");
        String newPws = new Md5().EncoderByMd5(params.get("newPws"));
        String recId = params.get("recId");
        StudentsEntity stu = info(recId);
        Md5 md5 = new Md5();
        //校验密码是否正确
        if (md5.checkpassword(oldPws, stu.getStuPassword())) {
            stu.setStuPassword(newPws);
            studentsService.updateById(stu);
            return R.ok();
        } else {
            return R.error(-9999, "请输入正确的密码");
        }

//
//        studentsService.updateById(students);


    }

    /**
     * 管理员修改学生信息
     */
    @RequestMapping("/admin/update")
    //@RequiresPermissions("user:students:update")
    public R adminUpdateStu(@RequestBody StudentsEntity students, @RequestHeader("token") String token) throws InterruptedException, UnsupportedEncodingException, NoSuchAlgorithmException {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        if (StringUtils.isNotEmpty(students.getStuPassword())){
            //重置密码进行MD5加密
            students.setStuPassword(new Md5().EncoderByMd5(students.getStuPassword()));
        }
        StudentsEntity studentsEntity1 = studentsService.getById(students.getRecId());
        if (students.getPosition() != studentsEntity1.getPosition()) {
            //创建消息类对象
            MessageUtli messageUtli = new MessageUtli(null, "您已被移除管理员身份", "移除管理员身份", null, null, DateUtils.getLocalDateTimeStr(),
                    null, null, studentsEntity1.getUserId(), studentsEntity1.getStuName(), null, 0);
            if (students.getPosition() != null && students.getPosition() == 1) {
                messageUtli.setContent("您已经被任命为社团管理员");
                messageUtli.setTitle("任命管理员通知");
            }
            UseradminassociatedEntity useradminassociatedEntity = studentToUseradmin(studentsEntity1);
            useradminassociatedService.save(useradminassociatedEntity);
        }
        studentsService.updateById(students);

        //同步更新redis信息
        StudentsEntity studentsEntity = studentsService.getById(students.getRecId());
        redisUtil.del(Constant.COMMUNITY_USER + studentsEntity.getUserId());
        getUserInfo(studentsEntity.getUserId());
        return R.ok();

    }

    //学生转学生管理员实体对象
    private UseradminassociatedEntity studentToUseradmin(StudentsEntity studentsEntity) {
        UseradminassociatedEntity useradminassociatedEntity = new UseradminassociatedEntity();
        useradminassociatedEntity.setDealTime(DateUtils.getLocalDateTimeStr());
        useradminassociatedEntity.setPosition(studentsEntity.getPosition());
        useradminassociatedEntity.setSno(studentsEntity.getUserId());
        useradminassociatedEntity.setUserName(studentsEntity.getStuName());
        return useradminassociatedEntity;
    }

    /**
     * 删除学生
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("user:students:delete")
    public R delete(@RequestBody String[] recIds, @RequestHeader("token") String token) {
        if (!token.equals(redisUtil.get(Constant.LOGIN_TOKEN + token))) {
            return R.error(Constant.TOKEN, "登录信息失效，请重新登陆");
        }
        studentsService.removeByIds(Arrays.asList(recIds));
        //同步删除redis信息
        for (int i = 0; i < recIds.length; i++) {
            String stuId = String.valueOf(redisUtil.get(Constant.COMMUNITY_USER_RECID + recIds[i]));
            redisUtil.del(Constant.COMMUNITY_USER + stuId);
            redisUtil.del(recIds[i]);
            redisUtil.del(Constant.COMMUNITY_USER_SCORE + stuId);
        }
        //批量删除redis
        return R.ok();
    }

    /**
     * 根据学号查询此学生相关消息
     */
    @RequestMapping("/info/userInfo/{userId}")
    public R getUserInfoByUserId(@PathVariable("userId") String userId) throws InterruptedException {
        return R.ok().put("data", getUserInfo(userId)).put("position",redisUtil.get(Constant.COMMUNITY_POSITION_USERID + userId));
    }

    /**
     * 先查redis  没有再查数据库
     *
     * @param userId
     * @return
     * @throws InterruptedException
     */
    public StudentsEntity getUserInfo(String userId) throws InterruptedException {
        //加的为正常的锁非读写锁
//        RLock lock = redissonClient.getLock("communityInfoLock");
        //添加写锁  防止缓存出现脏数据 防止缓存穿透

        RReadWriteLock readLock = redisson.getReadWriteLock("userInfoLock");
        RLock rLock = readLock.readLock();
        //设置 加锁时常   不设置默认为 看门狗  自动监控线程  30s解锁    设置时间要大于程序运行时间，不然出错
        rLock.lock(5, TimeUnit.SECONDS);
        //判断缓存是否有数据 有的话直接返回，没有的话去查库

        if (redisUtil.hasKey(Constant.COMMUNITY_USER + userId)) {
            String userRedisInfo = String.valueOf(redisUtil.get(Constant.COMMUNITY_USER + userId));
            StudentsEntity studentsEntity = JSON.parseObject(userRedisInfo, StudentsEntity.class);
            rLock.unlock();
            return studentsEntity;
        } else {
            QueryWrapper<StudentsEntity> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("USER_ID", userId);
            StudentsEntity studentsEntity = studentsService.getOne(queryWrapper);
            redisUtil.set(Constant.COMMUNITY_USER_RECID + studentsEntity.getRecId(), studentsEntity.getUserId(), 60 * 60 * 24 * 30 * 12 * 4);
            if (org.springframework.util.StringUtils.isEmpty(studentsEntity)) {
                //防止缓存穿透
                redisUtil.set(Constant.COMMUNITY_USER + userId, "null", 60 * 60 * 24 * 30 * 12 * 4);
                rLock.unlock();
                return null;
            }
            String s = JSON.toJSONString(ConUtils.beanToMap(studentsEntity));
            redisUtil.set(Constant.COMMUNITY_USER + userId, s, 60 * 60 * 24 * 30 * 12 * 4);
            rLock.unlock();
            return studentsEntity;
        }

    }

    /**
     * 学生或管理员退出登录
     */
    @RequestMapping("quiteLogin")
    //@RequiresPermissions("user:students:delete")
    public R quitLogin(@RequestHeader("token") String token) {
        redisUtil.del(Constant.LOGIN_TOKEN + token);
        return R.ok();
    }

    /**
     * 学生或普通管理员登录
     */
    @RequestMapping("stu/login")
    //@RequiresPermissions("user:students:delete")
    public R login(@RequestParam Map<String, Object> params) throws UnsupportedEncodingException, NoSuchAlgorithmException {
        String pws = String.valueOf(params.get("stuPassword"));
        pws = new Md5().EncoderByMd5(pws);
        //5分钟内输入10次
        String redisLoginCount = Constant.LOGIN_COUNT + params.get("userId");
        if (redisUtil.hasKey(redisLoginCount)) {
            redisUtil.incr(redisLoginCount, 1);
            redisUtil.expire(redisLoginCount, 60 * 5);
        } else {
            redisUtil.set(redisLoginCount, 0, 60 * 5);
        }
        if ((int) redisUtil.get(redisLoginCount) > 9) {
            redisUtil.expire(redisLoginCount, 60 * 5);
            return R.error(Constant.ERROR, "错误次数过多，请5分钟后重新尝试");
        }
        //匹配加密后的密码
        QueryWrapper<StudentsEntity> querwrapper = new QueryWrapper<>();
        querwrapper.eq("USER_ID", params.get("userId")).eq("STU_PASSWORD", pws);
        StudentsEntity studentsEntity = studentsService.getOne(querwrapper);
        if (studentsEntity == null) {
            return R.error(Constant.ERROR, "用户名或密码错误");
        }
        redisUtil.del(redisLoginCount);
        Map<String, String> map = new HashMap<>();
        map.put("userId", studentsEntity.getUserId());
        map.put("stuName", studentsEntity.getStuName());
        String token = JwtUtils.createJsonWebToken(map);
        //tocken3小时有效
        redisUtil.set(Constant.LOGIN_TOKEN + token, token, 60 * 60 * 3);
        return R.ok().put("token", token).put("data", studentsEntity).put("position", redisUtil.get(Constant.COMMUNITY_POSITION_USERID + studentsEntity.getUserId())).put("score", redisUtil.get(Constant.COMMUNITY_USER_SCORE + studentsEntity.getUserId()));
    }

}
