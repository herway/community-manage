package com.hww.communitymanage.user.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hww.communitymanage.user.entity.UseradminassociatedEntity;
import com.hww.communitymanage.user.service.UseradminassociatedService;
import com.hww.common.utils.PageUtils;
import com.hww.common.utils.R;



/**
 * 学生管理员关联表
 *
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@RestController
@RequestMapping("user/useradminassociated")
public class UseradminassociatedController {
    @Autowired
    private UseradminassociatedService useradminassociatedService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("user:useradminassociated:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = useradminassociatedService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{recId}")
    //@RequiresPermissions("user:useradminassociated:info")
    public R info(@PathVariable("recId") String recId){
		UseradminassociatedEntity useradminassociated = useradminassociatedService.getById(recId);

        return R.ok().put("useradminassociated", useradminassociated);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("user:useradminassociated:save")
    public R save(@RequestBody UseradminassociatedEntity useradminassociated){
		useradminassociatedService.save(useradminassociated);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("user:useradminassociated:update")
    public R update(@RequestBody UseradminassociatedEntity useradminassociated){
		useradminassociatedService.updateById(useradminassociated);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("user:useradminassociated:delete")
    public R delete(@RequestBody String[] recIds){
		useradminassociatedService.removeByIds(Arrays.asList(recIds));

        return R.ok();
    }

}
