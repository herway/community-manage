package com.hww.communitymanage.user.dao;

import com.hww.communitymanage.user.entity.StudentsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 学生表
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@Mapper
public interface StudentsDao extends BaseMapper<StudentsEntity> {
	
}
