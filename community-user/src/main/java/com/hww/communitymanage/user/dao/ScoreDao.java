package com.hww.communitymanage.user.dao;

import com.hww.communitymanage.user.entity.ScoreEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 学生分数审批表
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@Mapper
public interface ScoreDao extends BaseMapper<ScoreEntity> {
	
}
