package com.hww.communitymanage.user.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 学生表
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:42:33
 */
@Data
@TableName("user_students")
public class StudentsEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键id
	 */
	@TableId(type = IdType.ASSIGN_UUID)   //使用UUID策略来生成主键
	private String recId;
	/**
	 * 学号
	 */
	private String userId;
	/**
	 * 姓名
	 */
	private String stuName;
	/**
	 * 班级
	 */
	private String userClass;
	/**
	 * 性别
	 */
	private Integer sex;
	/**
	 * 密码
	 */
	private String stuPassword;
	/**
	 * 二课分
	 */
	private Double score;
	/**
	 * 手机号
	 */
	private String phone;
	/**
	 * 最后更新时间
	 */
	private String updateTime;
	/**
	 * 导入时间
	 */
	private String createTime;
    /**
     * 职位0学生1社团管理员
     */
    private Integer position;
    /**
     * 拓展字段1
     */
    @TableField(select = false)   //查询不返回此字段
    private String expand1;
    /**
     * 拓展字段2
     */
    @TableField(select = false)   //查询不返回此字段
    private String expand2;

}
