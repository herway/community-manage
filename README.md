# Graduation project
#### 背景
因学校没有成熟的社团管理系统，一为满足学校需求， 其次开源的社团管理项目，多为mvc ssm框架 很少有前后端分离的微服务框架，为了科技的发展，
更好学习解决高并发问题，也为了更好地掌握锤炼新技术，特开发此系统。
#### 项目展示
1. 学生端  项目展示地址：http://heww.cn   默认密码为学号后六位
2. 管理员端 项目展示地址： http://heww.cn:81 默认账号登录为管理员  超级管理员：admin 密码 123123 
3. 讲解视频：https://www.bilibili.com/video/BV1QF41147dj/
#### 软件架构
vue + spring boot + spring cloud + maven + nacos + getway + redis +  rabbitMQ + MyBatis-Plus
#### 安装教程
1.  前提环境： mysql + redis + nacos + rabbitMQ + jdk1.8
2.  开发工具  idea + navicat + VSCode + postMan + xshell + git
3.  此项目环境均使用 docker 部署在阿里云服务器上，像redis，nacos ,rabbitMQ 等组件,照片文件等存储用的阿里oss。
4.  前端使用 VUE + element组件 
#### 使用说明
1.  项目已开发完成，有细节问题或者好的建议欢迎邮箱指出。
2.  项目已基本完成，如需要帮助或者要前端源码、配置文件等，欢迎加QQ1341931964 咨询

#### 参与贡献

1.  框架环境搭建：赫威威
2.  代码编写及提交：赫威威
3.  业务逻辑及系统设计：赫威威
3.  前端VUE支持：陈东明


#### 特技
·······---------共勉--------------

            生如蝼蚁，当有鸿鹄之志。

            命如纸薄，应有不屈之心。

            大丈夫身居天地间，

            岂能郁郁久居人下。

            当以梦为马，不负韶华。
            
            乾坤未定，你我皆是黑马。


#### 声明
本程序仅为中原工学院，赫威威，陈东明使用，请尊重版权，未经允许不可用做商业用途，违者必究。

项目个别图片展示
![输入图片说明](community-activity/src/main/java/com/hww/communitymanage/activity/config/1.png)
![输入图片说明](community-activity/src/main/java/com/hww/communitymanage/activity/config/2.png)
![输入图片说明](community-activity/src/main/java/com/hww/communitymanage/activity/config/3.png)
![输入图片说明](community-activity/src/main/java/com/hww/communitymanage/activity/config/4.png)
![输入图片说明](community-activity/src/main/java/com/hww/communitymanage/activity/config/5.png)
![输入图片说明](community-activity/src/main/java/com/hww/communitymanage/activity/config/6.png)