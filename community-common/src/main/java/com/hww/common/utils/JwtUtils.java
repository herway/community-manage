package com.hww.common.utils;

//import io.jsonwebtoken.Claims;
//import io.jsonwebtoken.Jwts;
//import io.jsonwebtoken.SignatureAlgorithm;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.Map;

/**
 * jwt工具类
 */
public class JwtUtils {



    public static final long EXPIRE = 1000*60;  //过期时间，毫秒，一分钟

    //秘钥
    public static final  String APPSECRET = "c60b98a542615d7d26e3724f26356a47";

    /**
     * 生成jwt
     */
    public static String createJsonWebToken(Map<String,String> stu){

        if(stu == null ){
            return null;
        }
        String token = Jwts.builder()
                .claim("userId",stu.get("userId"))
                .claim("stuName",stu.get("stuName"))
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis()+EXPIRE))
                .signWith(SignatureAlgorithm.HS256,APPSECRET).compact();
        return token;
    }


    /**
     * 校验token
     */
    public static Claims checkJWT(String token ){
        try{
            final Claims claims =  Jwts.parser().setSigningKey(APPSECRET).
                    parseClaimsJws(token).getBody();
            return  claims;

        }catch (Exception e){ }
        return null;

    }



}