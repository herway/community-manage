package com.hww.common.utils;

import java.util.UUID;

/**
 * 常量
 *
 * @author hww
 * @email hww21@163.com
 * @date 2021年1月1日 上午12:07:22
 */
public class Constant {

    //状态码常量，0正常，错误-9999
    public static final Integer SUCCESS = 0;
    public static final Integer ERROR = -9999;
    //token错误状态码
    public static final Integer TOKEN = -8888;

    // ----------------redis常量存储---------------------
    /**
     * 社团模块
     */
    //社团
    public static final String COMMUNITY_ASSOCIATION = "COMMUNITY_ASSOCIATION_";
    //社团热度
    public static final String COMMUNITY_ASSOCIATION_HOT = "COMMUNITY_ASSOCIATION_HOT_";
    //社团活动中个数
    public static final String COMMUNITY_ACTIVITY_NUM = "COMMUNITY_ACTIVITY_NUM_";
    //社团当前举办活动数
    public static final String COMMUNITY_ACT_NUM = "COMMUNITY_ACT_NUM_";

    //社团学号和社长关联
    public static final String COMMUNITY_POSITION_USERID = "COMMUNITY_POSITION_USERID_";
    /**
     * 活动模块
     */
    //活动
    public static final String COMMUNITY_ACTIVITY = "COMMUNITY_ACTIVITY_";
    //活动开始报名时间
    public static final String COMMUNITY_ACTIVITY_NEW_BEGIN = "COMMUNITY_ACTIVITY_NEW_BEGIN_";
    //活动结束报名时间
    public static final String COMMUNITY_ACTIVITY_NEW_END = "COMMUNITY_ACTIVITY_NEW_END_";
    //活动开始时间
    public static final String COMMUNITY_ACTIVITY_NEW = "COMMUNITY_ACTIVITY_NEW_";
    //活动结束时间
    public static final String COMMUNITY_ACTIVITY_END = "COMMUNITY_ACTIVITY_END_";
    //活动热度
    public static final String COMMUNITY_ACTIVITY_HOT = "COMMUNITY_ACTIVITY_HOT_";
    //活动数
    public static final String ACTIVITY_NUM = "ACTIVITY_NUM_";

    /**
     * 用户模块
     */
    //用户
    public static final String COMMUNITY_USER = "COMMUNITY_USER_";
    //用户分数
    public static final String COMMUNITY_USER_SCORE = "COMMUNITY_USER_SCORE_";
    //用户主键和学号关联
    public static final String COMMUNITY_USER_RECID = "COMMUNITY_USER_RECID_";
    //存储登录token
    public static final String LOGIN_TOKEN = "LOGIN_TOKEN_";
    //存储同一账号登录次数
    public static final String LOGIN_COUNT = "LOGIN_COUNT_";
    //学生数
    public static final String STUDENT_NUM = "STUDENT_NUM_";
    /**
     * 超级管理员ID
     */

    /**
     * 消息模块
     */
    //未读消息
    public static final String NO_READ_MESSAGE = "NO_READ_MESSAGE_";
    //新消息
    public static final String NEW_MESSAGE = "NEW_MESSAGE_";

    public static final int SUPER_ADMIN = 1;
    /**
     * 当前页码
     */
    public static final String PAGE = "page";
    /**
     * 每页显示记录数
     */
    public static final String LIMIT = "limit";
    /**
     * 排序字段
     */
    public static final String ORDER_FIELD = "sidx";
    /**
     * 排序方式
     */
    public static final String ORDER = "order";
    /**
     * 升序
     */
    public static final String ASC = "asc";

    /**
     * 菜单类型
     *
     * @author hww
     * @email hww21@163.com
     * @date 2021年1月1日 上午12:07:22
     */
    public enum MenuType {
        /**
         * 目录
         */
        CATALOG(0),
        /**
         * 菜单
         */
        MENU(1),
        /**
         * 按钮
         */
        BUTTON(2);

        private int value;

        MenuType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 定时任务状态
     *
     * @author hww
     * @email hww21@163.com
     * @date 2021年1月1日 上午12:07:22
     */
    public enum ScheduleStatus {
        /**
         * 正常
         */
        NORMAL(0),
        /**
         * 暂停
         */
        PAUSE(1);

        private int value;

        ScheduleStatus(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /**
     * 云服务商
     */
    public enum CloudService {
        /**
         * 七牛云
         */
        QINIU(1),
        /**
         * 阿里云
         */
        ALIYUN(2),
        /**
         * 腾讯云
         */
        QCLOUD(3);

        private int value;

        CloudService(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }


}
