package com.hww.common.utils;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 
 * 
 * @author heweiwei
 * @email heww21@163.com
 * @date 2020-12-31 16:04:08
 */

@Data
public class MessageUtli {
	private static final long serialVersionUID = 1L;
	public MessageUtli(String communityId, String content, String title, String communityName, String acvitityId, String sendTime, String sendUserId, String sendName, String acceptUserId, String acceptName, String acvitityName, Integer stateId) {
		this.communityId = communityId;
		this.communityName = communityName;
		this.acvitityId = acvitityId;
		this.sendTime = sendTime;
		this.sendUserId = sendUserId;
		this.sendName = sendName;
		this.content = content;
		this.title = title;
		this.acceptUserId = acceptUserId;
		this.acceptName = acceptName;
		this.acvitityName = acvitityName;
		this.stateId = stateId;
	}
	private String recId;
	/**
	 * 社团ID
	 */
	private String communityId;
	/**
	 * 社团姓名
	 */
	private String communityName;
	/**
	 * 活动ID
	 */
	private String acvitityId;
	/**
	 * 消息发送时间
	 */
	private String sendTime;
	/**
	 * 消息接收时间
	 */
	private String acceptTime;
	/**
	 * 发送人ID
	 */
	private String sendUserId;
	/**
	 * 发送人姓名
	 */
	private String sendName;
	/**
	 * 收件人ID
	 */
	private String acceptUserId;
	/**
	 * 消息内容
	 */
	private String content;
	/**
	 * 消息标题
	 */
	private String title;
	/**
	 * 收件人姓名
	 */
	private String acceptName;
	/**
	 * 活动姓名
	 */
	private String acvitityName;
	/**
	 *明哥商议
	 */
	private Integer stateId;

}
